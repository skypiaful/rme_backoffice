import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { UserManagementRoutingModule } from './user-management-routing.module';
import { UserManagementComponent } from './user-management.component';
import { UserDetailComponent } from './user-detail/user-detail.component';
import { UserListComponent } from './user-list/user-list.component';
import { UserListItemComponent } from './user-list/user-list-item/user-list-item.component';

import { AlertDialogComponent } from '../../shared/modules/dialog/alert-dialog/alert-dialog.component';
import { AlertDialogModule } from '../../shared/modules/dialog/alert-dialog/alert-dialog.module';
import { YesNoDialogModule } from '../../shared/modules/dialog/yes-no-dialog/yes-no-dialog.module';
import { YesNoDialogComponent } from '../../shared/modules/dialog/yes-no-dialog/yes-no-dialog.component';
import { PageHeaderModule } from '../../shared';
import { MatDialogModule } from '@angular/material/dialog';
import { DeleteDialogModule } from '../../shared/modules/dialog/delete-dialog/delete-dialog.module';
import { DeleteDialogComponent } from '../../shared/modules/dialog/delete-dialog/delete-dialog.component';
import {
  MatInputModule,
  MatPaginatorModule,
  MatProgressSpinnerModule,
  MatSortModule,
  MatTableModule,
  MatCheckboxModule,
  MatButtonModule,
  MatIconModule,
  MatSlideToggleModule
} from '@angular/material';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    UserManagementRoutingModule,
    CommonModule,
    FormsModule,
    PageHeaderModule,
    MatInputModule,
    MatTableModule,
    MatIconModule,
    MatPaginatorModule,
    MatSortModule,
    MatProgressSpinnerModule,
    MatCheckboxModule,
    MatButtonModule,
    MatDialogModule,
    MatSlideToggleModule,
    DeleteDialogModule,
    AlertDialogModule,
    YesNoDialogModule
  ],
  declarations: [UserManagementComponent, UserDetailComponent, UserListComponent, UserListItemComponent]
})
export class UserManagementModule { }
