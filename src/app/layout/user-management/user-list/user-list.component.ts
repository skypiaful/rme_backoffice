import { Component, OnInit, OnDestroy } from '@angular/core';
import { User } from '../../../models/user.model';
import { Subscription } from 'rxjs';
import { UserService, UserCriteria } from '../../../shared/services/user/user.service';
import { Pagination } from '../../../models/pagination';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit, OnDestroy {

  term;

  private activeUser;

  private onUsersChanged: Subscription;

  public page: Pagination<User>;

  constructor(
    private userService: UserService
  ) { }

  ngOnInit() {
    this.onUsersChanged = this.userService.onUsersChanged.subscribe(data => {
      this.page = data;
      this.userService.onUserChanged.next(null);
    });
  }

  onTermKeypress(event) {
    if (event.code === 'Enter') {
      this.searchByTerm();
    }
  }

  nextPage(event) {
    this.userService.criteria.page++;
    this.userService.getUsers().subscribe();
  }

  prevPage(event) {
    this.userService.criteria.page--;
    this.userService.getUsers().subscribe();
  }

  searchByTerm() {
    this.userService.criteria.page = 1;
    this.userService.criteria.term = this.term;
    this.userService.getUsers().subscribe();
  }

  selectUser(user) {
    this.activeUser = user;
    this.userService.onUserChanged.next(user);
  }

  deleteUser(user) {
    this.userService.delete(user.id).subscribe(data => {
      this.userService.getUsers().subscribe();
    });
  }

  ngOnDestroy() {
    this.onUsersChanged.unsubscribe();
  }
}
