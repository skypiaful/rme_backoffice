import { Component, OnInit } from '@angular/core';
import { UserService } from '../../../../shared/services/user/user.service';

@Component({
  selector: 'app-user-list-item',
  templateUrl: './user-list-item.component.html',
  styleUrls: ['./user-list-item.component.scss']
})
export class UserListItemComponent implements OnInit {

  constructor(
    private userService: UserService
  ) { }

  ngOnInit() {
  }

  deleteUser(event, id) {
    this.userService.delete(id);
  }

}
