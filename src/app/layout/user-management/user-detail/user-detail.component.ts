import { Component, OnInit, OnDestroy } from '@angular/core';
import { UserService } from '../../../shared/services/user/user.service';
import { Subscription } from 'rxjs';
import { User } from '../../../models/user.model';
import { FormGroup, Validators, FormControl } from '@angular/forms';

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.scss']
})
export class UserDetailComponent implements OnInit, OnDestroy {

  private onUserChanged: Subscription;

  userForm: FormGroup;
  user: User;

  constructor(
    private userService: UserService
  ) { }

  ngOnInit() {
    this.user = null;
    this.onUserChanged = this.userService.onUserChanged.subscribe(data => {
      this.user = data;
    });

    /** Init form validator */
    // this.userForm = new FormGroup({
    //   'firstName': new FormControl(this.user.firstName, [
    //     Validators.required,
    //     Validators.minLength(4)
    //   ]),
    //   'phoneNo': new FormControl(this.user.phoneNo, Validators.required)
    // });
  }

  saveUser(event) {
    this.userService.update(this.user).subscribe(data => {
      return data;
    });
  }

  createUser(event) {
    this.userService.create(this.user).subscribe(data => {
      return data;
    });
  }

  deselectUser(event) {
    this.userService.onUserChanged.next(null);
  }

  ngOnDestroy() {
    this.onUserChanged.unsubscribe();
  }

  onSelectFile(event) {
    if (event.target.files && event.target.files[0]) {
      const reader = new FileReader();

      reader.readAsDataURL(event.target.files[0]); // read file as data url

      reader.onload = (e) => { // called once readAsDataURL is completed
        this.user.avatar = e.target['result'];
        console.log(this.user.avatar);
      };
    }
  }

}
