import { Component, OnInit } from '@angular/core';
import { UserService } from '../../shared/services/user/user.service';
import { User } from '../../models/user.model';

@Component({
  selector: 'app-user-management',
  templateUrl: './user-management.component.html',
  styleUrls: ['./user-management.component.scss']
})
export class UserManagementComponent implements OnInit {
  public loading$ = this.userService.loading$;

  public user: User;

  constructor(
    private userService: UserService
  ) { }

  ngOnInit() {
    this.userService.getUsers().subscribe(data => {
      return data;
    });

    this.userService.onUserChanged.subscribe(data => {
      this.user = data;
    });
  }

}
