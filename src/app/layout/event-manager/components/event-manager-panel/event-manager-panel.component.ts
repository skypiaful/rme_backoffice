import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { EventService } from '../../../../shared/services/event/event.service';
import { MatDialog, MatSelect } from '@angular/material';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Event } from '../../../../models/event.model';
import { BehaviorSubject, of, Subject } from 'rxjs';
import { BoothService } from '../../../../shared/services/booths/booth.service';
import { YesNoDialogComponent } from '../../../../shared/modules/dialog/yes-no-dialog/yes-no-dialog.component';
import { catchError, finalize, takeUntil } from 'rxjs/operators';
import { AlertDialogComponent } from '../../../../shared/modules/dialog/alert-dialog/alert-dialog.component';
import { Place } from '../../../../models/place.model';
import { PlaceService } from '../../../../shared/services/place/place.service';
import { Booth } from '../../../../models/booth.model';
import { DeleteDialogComponent } from '../../../../shared/modules/dialog/delete-dialog/delete-dialog.component';
import { Promotion } from '../../../../models/promotion.model';
import { PromotionService } from '../../../../shared/services/promotion/promotion.service';
import { ActivatedRoute } from '@angular/router';
import { toInteger } from '@ng-bootstrap/ng-bootstrap/util/util';

@Component({
  selector: 'app-event-manager-panel',
  templateUrl: './event-manager-panel.component.html',
  styleUrls: ['./event-manager-panel.component.scss']
})
export class EventManagerPanelComponent implements OnInit, OnDestroy {

  private eventSubject = new BehaviorSubject<Event>(null);
  private placeSubject = new BehaviorSubject<Place>(null);
  private boothSubject = new BehaviorSubject<Booth>(null);

  private promotionSubject = new BehaviorSubject<Promotion>(null);

  private deleteBoothSubject = new BehaviorSubject<Booth>(null);
  private deletePromotionSubject = new BehaviorSubject<Promotion>(null);

  private showAddBoothSubject = new BehaviorSubject<boolean>(false);
  private showAddPromotionSubject = new BehaviorSubject<boolean>(false);
  private showEditPromotionSubject = new BehaviorSubject<boolean>(false);
  private showCreateEventSubject = new BehaviorSubject<boolean>(false);
  private onHasPlaceSubject = new BehaviorSubject<boolean>(false);

  private loadingSubject = new BehaviorSubject<boolean>(false);
  private showEditEventSubject = new BehaviorSubject<boolean>(false);
  private showChoicePlaceSubject = new BehaviorSubject<boolean>(false);
  /** list of banks filtered by search keyword */
  public filteredPlaces: BehaviorSubject<Place[]> = new BehaviorSubject<Place[]>([]);

  public hasPlace$ = this.onHasPlaceSubject.asObservable();
  public addbooth$ = this.showAddBoothSubject.asObservable();
  public addpromotion$ = this.showAddPromotionSubject.asObservable();
  public editpromotion$ = this.showEditPromotionSubject.asObservable();
  public choicePlace$ = this.showChoicePlaceSubject.asObservable();
  public loading$ = this.loadingSubject.asObservable();
  public editEvent$ = this.showEditEventSubject.asObservable();
  public createEvent$ = this.showCreateEventSubject.asObservable();

  /** control for the MatSelect filter keyword */
  public placeFilterCtrl: FormControl = new FormControl();
  eventCreatorForm: FormGroup = this.fb.group({
    'place': [null, Validators.required],
    'name': ['', Validators.required],
    'description': [null],
    'status': false
  });
  boothCreatorForm: FormGroup;
  promotionCreatorForm: FormGroup;
  activeStatus: string;
  placeCtrl: FormControl;
  places: Place[] = [];
  currentEvent: Event;
  currentPlace: Place;
  currentBooth: Booth;
  promotions: Promotion[];
  currentPromotion: Promotion;

  /** Subject that emits when the component has been destroyed. */
  private _onDestroy = new Subject<void>();

  @ViewChild('singleSelect') singleSelect: MatSelect;
  constructor(
    private eventService: EventService,
    private boothService: BoothService,
    private placeService: PlaceService,
    private promotionService: PromotionService,
    private _matDialog: MatDialog,
    private fb: FormBuilder,
    private route: ActivatedRoute) {
    this.placeCtrl = new FormControl();
  }

  ngOnInit() {
    // this.showChoicePlaceSubject.next(true);
    // this.showCreateEventSubject.next(true);
    // set initial selection
    // listen for search field value changes
    this.placeFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        if (this.placeFilterCtrl.value !== '') {
          this.search(this.placeFilterCtrl.value);
        }
      });

    this.filteredPlaces.subscribe((places) => {
      // if (this.singleSelect !== undefined && this.singleSelect !== null) {
      //   this.singleSelect.compareWith = (a: Place, b: Place) => a.id === b.id;
      // }
      this.places = places;
    });

    this.placeFilterCtrl.setValue('');

    this.eventSubject.subscribe((event) => {
      this.currentEvent = event;
      if (event !== undefined && event !== null && event.id !== undefined && event.id !== null) {
        this.placeService.getPlaceById(event.placeId).subscribe((res) => {
          this.placeSubject.next(res);
        });
      }
    });

    this.placeSubject.subscribe((place) => {
      this.currentPlace = place as Place;
      this.createForm(this.currentEvent);
      if (this.currentPlace !== null && this.currentPlace !== undefined) {
        this.onHasPlaceSubject.next(true);
        this.showEditEventSubject.next(true);
        this.showChoicePlaceSubject.next(false);
        this.showCreateEventSubject.next(false);
      }
    });

    this.promotionSubject.subscribe((promotion) => {
      if (promotion !== undefined && promotion !== null) {
        if (this.currentBooth.promotions === undefined || this.currentBooth.promotions === null) {
          this.currentBooth.promotions = [];
        }
        this.currentBooth.promotions.push(promotion as Promotion);
      }
    });

    this.boothSubject.subscribe((booth) => {
      if (booth !== undefined && booth !== null) {
        if (this.currentEvent.booths === undefined || this.currentEvent.booths === null) {
          this.currentEvent.booths = [];
        }
        this.currentEvent.booths.push(booth as Booth);
      }
    });

    this.deleteBoothSubject.subscribe((booth) => {
      if (booth !== undefined && booth !== null) {
        if (this.currentEvent.booths === undefined || this.currentEvent.booths === null) {
          this.currentEvent.booths = [];
        }
        const index = this.currentEvent.booths.indexOf(booth as Booth);
        this.currentEvent.booths.splice(index, 1);
      }
    });

    this.deletePromotionSubject.subscribe((promotion) => {
      if (promotion !== undefined && promotion !== null) {
        if (this.currentBooth.promotions === undefined || this.currentBooth.promotions === null) {
          this.currentBooth.promotions = [];
        }
        const index = this.currentBooth.promotions.indexOf(promotion as Promotion);
        this.currentBooth.promotions.splice(index, 1);
      }
    });

    const id = toInteger(this.route.snapshot.paramMap.get('id'));
    if (id !== undefined && id !== null && !isNaN(id)) {
      this.eventService.getEventById(id).subscribe((res) => {
        this.eventSubject.next(res);
      });
    } else {
      this.createForm(null);
      this.onHasPlaceSubject.next(false);
      this.showEditEventSubject.next(false);
      this.showCreateEventSubject.next(true);
      this.showChoicePlaceSubject.next(true);
    }
    this.search('');
  }

  onClickBooth(booth: Booth) {
    this.currentBooth = booth;
    this.showAddPromotionSubject.next(true);
    this.showEditPromotionSubject.next(false);
  }

  onPromotionClick(promotion: Promotion) {
    this.currentPromotion = promotion;
    this.showEditPromotionSubject.next(true);
  }

  search(keyword: string) {
    this.placeService.getPlaces(keyword, 'id', 'desc', 0, 10).subscribe((res) => {
      this.places = res.data;
      this.filteredPlaces.next(this.places);
    });
  }

  onSavePromotionClick(value: any) {
    this.loadingSubject.next(true);
    this.currentPromotion.description = value.description;

    if (value.isActive) {
      this.currentPromotion.status = 'A';
    } else {
      this.currentPromotion.status = 'D';
    }

    if (this.currentPromotion != null) {
      this._matDialog.open(YesNoDialogComponent, {
        data: { head: 'Warning.', content: 'Do you want to save ' + this.currentPromotion.name + '?' }
      }).afterClosed().subscribe(result => {
        if (result === 'yes') {
          this.loadingSubject.next(true);
          this.promotionService.edit(this.currentPromotion).pipe(
            catchError((res) => of([
              this._matDialog.open(AlertDialogComponent, {
                data: { head: 'Error.', content: 'Cannot Create ' + this.currentPromotion.name + ' .', description: res.error }
              })
            ])),
            finalize(() => {
              this.loadingSubject.next(false);
            })
          ).subscribe((res) => {
            if ((res as Promotion).name !== undefined) {
              this._matDialog.open(AlertDialogComponent, {
                data: { head: 'Complete.', content: 'Create ' + (res as Promotion).name + ' Complete.' }
              });
            }
          });
        } else {
          this.loadingSubject.next(false);
        }
      });
    }
  }

  onDeleteClick(list) {
    list.selectedOptions.selected.map(item => item.value);
  }

  deleteBooth(booth: Booth): void {
    const dialogRef = this._matDialog.open(DeleteDialogComponent, {
      data: booth
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result !== 'Cancel' && (result as Booth) != null) {
        const name = (result as Booth).name;
        this.boothService.delete((result as Booth).id).pipe(
          catchError(() => of([
            this._matDialog.open(AlertDialogComponent, {
              data: { head: 'Delete Error.', content: 'Cannot Delete ' + name + '.' }
            })
          ])),
          finalize(() => {
            this.loadingSubject.next(false);
          })
        ).subscribe((res) => {
          if (res === 204) {
            this._matDialog.open(AlertDialogComponent, {
              data: { head: 'Delete Complete.', content: 'Delete ' + name + ' Complete.' }
            });
            this.deleteBoothSubject.next(booth);
          }
        });
      }
    });
  }

  deletePromotion(promotion: Promotion): void {
    const dialogRef = this._matDialog.open(DeleteDialogComponent, {
      data: promotion
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result !== 'Cancel' && (result as Promotion) != null) {
        const name = (result as Promotion).name;
        this.promotionService.delete((result as Promotion).id).pipe(
          catchError(() => of([
            this._matDialog.open(AlertDialogComponent, {
              data: { head: 'Delete Error.', content: 'Cannot Delete ' + name + '.' }
            })
          ])),
          finalize(() => {
            this.loadingSubject.next(false);
          })
        ).subscribe((res) => {
          if (res === 204) {
            this._matDialog.open(AlertDialogComponent, {
              data: { head: 'Delete Complete.', content: 'Delete ' + name + ' Complete.' }
            });
            this.deletePromotionSubject.next(promotion);
          }
        });
      }
    });
  }


  addBooth(value: any) {
    this.loadingSubject.next(true);
    const boothAdd = value as Booth;
    boothAdd.no = '-1';
    boothAdd.eventId = this.currentEvent.id;
    if (value.isActive) {
      boothAdd.status = 'A';
    } else {
      boothAdd.status = 'D';
    }

    if (boothAdd != null) {
      this._matDialog.open(YesNoDialogComponent, {
        data: { head: 'Warning.', content: 'Do you want to create ' + boothAdd.name + '?' }
      }).afterClosed().subscribe(result => {
        if (result === 'yes') {
          this.loadingSubject.next(true);
          this.boothService.create(boothAdd).pipe(
            catchError((res) => of([
              this._matDialog.open(AlertDialogComponent, {
                data: { head: 'Error.', content: 'Cannot Create ' + boothAdd.name + ' .', description: res.error }
              })
            ])),
            finalize(() => {
              this.loadingSubject.next(false);
            })
          ).subscribe((res) => {
            if ((res as Booth).name !== undefined) {
              this._matDialog.open(AlertDialogComponent, {
                data: { head: 'Complete.', content: 'Create ' + (res as Booth).name + ' Complete.' }
              });
            }
            this.boothSubject.next(res as Booth);
          });
        } else {
          this.loadingSubject.next(false);
        }
      });
    }
  }

  addPromotion(value: any) {
    this.loadingSubject.next(true);
    const promotionAdd = value as Promotion;
    promotionAdd.boothId = this.currentBooth.id;
    promotionAdd.eventId = this.currentBooth.id;

    if (value.isActive) {
      promotionAdd.status = 'A';
    } else {
      promotionAdd.status = 'D';
    }

    if (promotionAdd != null) {
      this._matDialog.open(YesNoDialogComponent, {
        data: { head: 'Warning.', content: 'Do you want to create ' + promotionAdd.name + '?' }
      }).afterClosed().subscribe(result => {
        if (result === 'yes') {
          this.loadingSubject.next(true);
          this.promotionService.create(promotionAdd).pipe(
            catchError((res) => of([
              this._matDialog.open(AlertDialogComponent, {
                data: { head: 'Error.', content: 'Cannot Create ' + promotionAdd.name + ' .', description: res.error }
              })
            ])),
            finalize(() => {
              this.loadingSubject.next(false);
            })
          ).subscribe((res) => {
            if ((res as Promotion).name !== undefined) {
              this._matDialog.open(AlertDialogComponent, {
                data: { head: 'Complete.', content: 'Create ' + (res as Promotion).name + ' Complete.' }
              });
            }
            this.promotionSubject.next(res as Promotion);
          });
        } else {
          this.loadingSubject.next(false);
        }
      });
    }
  }

  save(value: any) {
    this.loadingSubject.next(true);
    const eventAdd = value as Event;
    eventAdd.id = this.currentEvent.id;
    const place = this.currentPlace.id;
    eventAdd.placeId = place;
    if (value.status) {
      eventAdd.status = 'A';
    } else {
      eventAdd.status = 'D';
    }

    this.currentEvent = eventAdd;
    if (this.currentEvent != null) {
      this._matDialog.open(YesNoDialogComponent, {
        data: { head: 'Warning.', content: 'Do you want to save ' + this.currentEvent.name + '?' }
      }).afterClosed().subscribe(result => {
        if (result === 'yes') {
          this.loadingSubject.next(true);
          this.eventService.edit(this.currentEvent).pipe(
            catchError((res) => of([
              this._matDialog.open(AlertDialogComponent, {
                data: { head: 'Error.', content: 'Cannot Save ' + this.currentEvent.name + ' .', description: res.error }
              }),
              this.showEditEventSubject.next(true),
              this.showCreateEventSubject.next(false),
              this.showAddBoothSubject.next(false),
            ])),
            finalize(() => {
              this.loadingSubject.next(false);
            })
          ).subscribe((res) => {
            if ((res as Event).name !== undefined) {
              this._matDialog.open(AlertDialogComponent, {
                data: { head: 'Complete.', content: 'Save ' + (res as Event).name + ' Complete.' }
              });
            }
            this.showEditEventSubject.next(true);
            this.showCreateEventSubject.next(false);
            this.showAddBoothSubject.next(true);
            this.eventSubject.next((res as Event));
          });
        } else {
          this.loadingSubject.next(false);
        }
      });
    }
  }

  create(value: any) {
    this.loadingSubject.next(true);
    const eventAdd = value as Event;
    const place = value.place as Place;
    eventAdd.placeId = place.id;
    if (value.status) {
      eventAdd.status = 'A';
    } else {
      eventAdd.status = 'D';
    }

    this.currentEvent = eventAdd;
    if (this.currentEvent != null) {
      this._matDialog.open(YesNoDialogComponent, {
        data: { head: 'Warning.', content: 'Do you want to create ' + this.currentEvent.name + '?' }
      }).afterClosed().subscribe(result => {
        if (result === 'yes') {
          this.loadingSubject.next(true);
          this.eventService.create(this.currentEvent).pipe(
            catchError((res) => of([
              this._matDialog.open(AlertDialogComponent, {
                data: { head: 'Error.', content: 'Cannot Create ' + this.currentEvent.name + ' .', description: res.error }
              }),
              this.showEditEventSubject.next(false),
              this.showCreateEventSubject.next(true),
              this.showAddBoothSubject.next(false),
            ])),
            finalize(() => {
              this.loadingSubject.next(false);
            })
          ).subscribe((res) => {
            if ((res as Event).name !== undefined) {
              this._matDialog.open(AlertDialogComponent, {
                data: { head: 'Complete.', content: 'Create ' + (res as Event).name + ' Complete.' }
              });
            }
            this.showEditEventSubject.next(true);
            this.showCreateEventSubject.next(false);
            this.showAddBoothSubject.next(true);
            this.eventSubject.next((res as Event));
          });
        } else {
          this.loadingSubject.next(false);
        }
      });
    }
  }

  createForm(event: Event) {
    this.showEditEventSubject.next(true);
    if (event !== null) {
      this.eventCreatorForm = this.fb.group({
        'place': [this.currentPlace.name, Validators.required],
        'name': [event.name, Validators.required],
        'description': [event.description],
        'status': (event.status === 'A' ? true : false)
      });
      if (event.status === 'A' ? true : false) {
        this.activeStatus = 'Active';
      } else {
        this.activeStatus = 'Inactive';
      }

      this.boothCreatorForm = this.fb.group({
        'name': [null, Validators.required]
      });

      this.promotionCreatorForm = this.fb.group({
        'name': [null, Validators.required]
      });
      this.showAddBoothSubject.next(true);
    } else {
      this.showAddBoothSubject.next(false);
      this.eventCreatorForm = this.fb.group({
        'place': [null, Validators.required],
        'name': [null, Validators.required],
        'description': [null],
        'status': false
      });
      this.activeStatus = 'Inactive';

      this.boothCreatorForm = this.fb.group({
        'name': [null, Validators.required],
        'description': [null]
      });
      this.promotionCreatorForm = this.fb.group({
        'name': [null, Validators.required]
      });
    }
  }

  onActiveChange(value) {
    if (value.checked === true) {
      this.activeStatus = 'Active';
    } else {
      this.activeStatus = 'Inactive';
    }
  }

  ngOnDestroy() {
    this._onDestroy.next();
    this._onDestroy.complete();
  }
}
