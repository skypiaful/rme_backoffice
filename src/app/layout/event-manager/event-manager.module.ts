import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EventManagerRoutingModule } from './event-manager-routing.module';
import { EventManagerComponent } from './event-manager.component';
import { PageHeaderModule } from '../../shared';
import {
  MatInputModule,
  MatTableModule,
  MatIconModule,
  MatPaginatorModule,
  MatSortModule,
  MatProgressSpinnerModule,
  MatCheckboxModule,
  MatButtonModule,
  MatDialogModule,
  MatSlideToggleModule,
  MatListModule,
  MatAutocompleteModule,
  MatSelectModule
} from '@angular/material';
import { DeleteDialogModule } from '../../shared/modules/dialog/delete-dialog/delete-dialog.module';
import { AlertDialogModule } from '../../shared/modules/dialog/alert-dialog/alert-dialog.module';
import { YesNoDialogModule } from '../../shared/modules/dialog/yes-no-dialog/yes-no-dialog.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DeleteDialogComponent } from '../../shared/modules/dialog/delete-dialog/delete-dialog.component';
import { AlertDialogComponent } from '../../shared/modules/dialog/alert-dialog/alert-dialog.component';
import { YesNoDialogComponent } from '../../shared/modules/dialog/yes-no-dialog/yes-no-dialog.component';
import { EventListComponent } from './event-list/event-list.component';
import { EventCreatorComponent } from './event-creator/event-creator.component';
import { EventEditorComponent } from './event-editor/event-editor.component';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { EventManagerPanelComponent } from './components/event-manager-panel/event-manager-panel.component';

@NgModule({
  imports: [
    CommonModule,
    EventManagerRoutingModule,
    PageHeaderModule,
    MatInputModule,
    MatTableModule,
    MatIconModule,
    MatPaginatorModule,
    MatSortModule,
    MatProgressSpinnerModule,
    MatCheckboxModule,
    MatButtonModule,
    MatDialogModule,
    MatSlideToggleModule,
    DeleteDialogModule,
    AlertDialogModule,
    YesNoDialogModule,
    MatListModule,
    FormsModule,
    ReactiveFormsModule,
    MatAutocompleteModule,
    MatSelectModule,
    NgxMatSelectSearchModule
  ],
  entryComponents: [
    DeleteDialogComponent,
    AlertDialogComponent,
    YesNoDialogComponent
  ],
  declarations: [
    EventManagerComponent,
    EventListComponent,
    EventCreatorComponent,
    EventEditorComponent,
    EventManagerPanelComponent
  ]
})
export class EventManagerModule { }
