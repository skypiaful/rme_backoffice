import { Component, OnInit, AfterViewInit, ViewChild, ElementRef, ChangeDetectorRef, NgModule } from '@angular/core';
import { routerTransition } from '../../../router.animations';
import { EventService } from '../../../shared/services/event/event.service';
import { merge, fromEvent, of } from 'rxjs';
import { Event, } from '../../../models/event.model';
import { EventDataSource } from '../../../shared/services/event/event.datasource';
import { tap, debounceTime, distinctUntilChanged, catchError, finalize } from 'rxjs/operators';
import {
  MatPaginator,
  MatSort,
  MatDialog} from '@angular/material';

import { DeleteDialogComponent } from '../../../shared/modules/dialog/delete-dialog/delete-dialog.component';
import { AlertDialogComponent } from '../../../shared/modules/dialog/alert-dialog/alert-dialog.component';

@Component({
  selector: 'app-event-list',
  templateUrl: './event-list.component.html',
  styleUrls: ['./event-list.component.scss'],
  animations: [routerTransition()]
})

export class EventListComponent implements OnInit, AfterViewInit {
  displayedColumns = ['id', 'name', 'description', 'status', 'controller'];
  resultsLength = 0;
  isLoadingResults = false;
  isRateLimitReached = true;
  dataSource: EventDataSource;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('input') input: ElementRef;

  constructor(private eventService: EventService, public _matDialog: MatDialog) { }

  ngOnInit() {
    this.dataSource = new EventDataSource(this.eventService);
    this.input.nativeElement.value = '';
    this.sort.active = 'id';
    this.sort.direction = 'desc';
    this.paginator.pageSize = 10;
    this.paginator.pageIndex = 0;
    this.loadEventsPage();
  }

  deleteEvent(event: Event): void {
    const dialogRef = this._matDialog.open(DeleteDialogComponent, {
      data: event
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result !== 'Cancel' && (result as Event) != null) {
        const name = (result as Event).name;
        this.dataSource.delete((result as Event).id).pipe(
          catchError(() => of([
            this._matDialog.open(AlertDialogComponent, {
              data: { head: 'Delete Error.', content: 'Cannot Delete ' + name + '.' }
            })
          ])),
          finalize(() => {
            this.dataSource.loadingSubject.next(false);
          })
        ).subscribe((res) => {
          if (res === 204) {
            this._matDialog.open(AlertDialogComponent, {
              data: { head: 'Delete Complete.', content: 'Delete ' + name + ' Complete.' }
            });
          }
          this.loadEventsPage();
        });
      }
    });
  }

  loadEventsPage(): void {
    this.dataSource.loadEventsWithPaginator(
      this.input.nativeElement.value,
      this.sort.active,
      this.sort.direction,
      this.paginator);
  }

  ngAfterViewInit(): void {
    // server-side search
    fromEvent(this.input.nativeElement, 'keyup')
      .pipe(
        debounceTime(500),
        distinctUntilChanged(),
        tap(() => {
          this.paginator.pageIndex = 0;
          this.loadEventsPage();
        })
      )
      .subscribe();

    // reset the paginator after sorting
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

    // on sort or paginate events, load a new page
    merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        tap(() => this.loadEventsPage())
      )
      .subscribe();
  }
}
