import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EventManagerComponent } from './event-manager.component';
import { EventListComponent } from './event-list/event-list.component';
import { EventEditorComponent } from './event-editor/event-editor.component';
import { EventCreatorComponent } from './event-creator/event-creator.component';

const routes: Routes = [
  {
    path: '',
    component: EventManagerComponent,
    children: [
       { path: '', redirectTo: 'event-list' },
       { path: 'event-list', component: EventListComponent },
       { path: 'event-creator', component: EventCreatorComponent },
       { path: 'event-editor/:id', component: EventEditorComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EventManagerRoutingModule { }
