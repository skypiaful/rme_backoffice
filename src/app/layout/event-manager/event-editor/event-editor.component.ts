import { Component, OnInit } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Booth } from '../../../models/booth.model';

@Component({
  selector: 'app-event-editor',
  templateUrl: './event-editor.component.html',
  styleUrls: ['./event-editor.component.scss']
})
export class EventEditorComponent implements OnInit {
  private eventSubject = new BehaviorSubject<Event>(null);
  private loadingSubject = new BehaviorSubject<boolean>(false);
  private boothSubject = new BehaviorSubject<Booth>(null);
  constructor() { }

  ngOnInit() {
  }

}
