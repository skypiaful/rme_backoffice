import { Component, OnInit, AfterViewInit, ViewChild, ElementRef, ChangeDetectorRef, NgModule, OnDestroy } from '@angular/core';
import { routerTransition } from '../../../router.animations';
import { PlaceService } from '../../../shared/services/place/place.service';
import { merge, fromEvent, of } from 'rxjs';
import { Place, } from '../../../models/place.model';
import { PlaceDataSource } from '../../../shared/services/place/place.datasource';
import { tap, debounceTime, distinctUntilChanged, catchError, finalize } from 'rxjs/operators';
import {
  MatPaginator,
  MatSort,
  MatDialog} from '@angular/material';

import { DeleteDialogComponent } from '../../../shared/modules/dialog/delete-dialog/delete-dialog.component';
import { AlertDialogComponent } from '../../../shared/modules/dialog/alert-dialog/alert-dialog.component';

@Component({
  selector: 'app-place-list',
  templateUrl: './place-list.component.html',
  styleUrls: ['./place-list.component.scss'],
  animations: [routerTransition()]
})

export class PlaceListComponent implements OnInit, AfterViewInit, OnDestroy {

  displayedColumns = ['id', 'name', 'address', 'status', 'controller'];
  resultsLength = 0;
  isLoadingResults = false;
  isRateLimitReached = true;
  dataSource: PlaceDataSource;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('input') input: ElementRef;

  constructor(private placeService: PlaceService, public _matDialog: MatDialog) { }

  ngOnInit() {
    this.dataSource = new PlaceDataSource(this.placeService);
    this.input.nativeElement.value = '';
    this.sort.active = 'id';
    this.sort.direction = 'desc';
    this.paginator.pageSize = 10;
    this.paginator.pageIndex = 0;
    this.loadPlacesPage();
  }

  deletePlace(place: Place): void {
    const dialogRef = this._matDialog.open(DeleteDialogComponent, {
      data: place
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result !== 'Cancel' && (result as Place) != null) {
        const name = (result as Place).name;
        this.dataSource.delete((result as Place).id).pipe(
          catchError(() => of([
            this._matDialog.open(AlertDialogComponent, {
              data: { head: 'Delete Error.', content: 'Cannot Delete ' + name + '.' }
            })
          ])),
          finalize(() => {
            this.dataSource.loadingSubject.next(false);
          })
        ).subscribe((res) => {
          if (res === 204) {
            this._matDialog.open(AlertDialogComponent, {
              data: { head: 'Delete Complete.', content: 'Delete ' + name + ' Complete.' }
            });
          }
          this.loadPlacesPage();
        });
      }
    });
  }

  loadPlacesPage(): void {
    this.dataSource.loadPlacesWithPaginator(
      this.input.nativeElement.value,
      this.sort.active,
      this.sort.direction,
      this.paginator);
  }

  ngAfterViewInit(): void {
    // server-side search
    fromEvent(this.input.nativeElement, 'keyup')
      .pipe(
        debounceTime(500),
        distinctUntilChanged(),
        tap(() => {
          this.paginator.pageIndex = 0;
          this.loadPlacesPage();
        })
      )
      .subscribe();

    // reset the paginator after sorting
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

    // on sort or paginate events, load a new page
    merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        tap(() => this.loadPlacesPage())
      )
      .subscribe();
  }

  ngOnDestroy(): void {
    this.dataSource.disconnect(null);
    this.sort.sortChange.unsubscribe();
  }
}
