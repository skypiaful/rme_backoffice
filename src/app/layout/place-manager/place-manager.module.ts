import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { PlaceManagerRoutingModule } from './place-manager-routing.module';
import { PlaceManagerComponent } from './place-manager.component';
import { AlertDialogComponent } from '../../shared/modules/dialog/alert-dialog/alert-dialog.component';
import { AlertDialogModule } from '../../shared/modules/dialog/alert-dialog/alert-dialog.module';
import { YesNoDialogModule } from '../../shared/modules/dialog/yes-no-dialog/yes-no-dialog.module';
import { YesNoDialogComponent } from '../../shared/modules/dialog/yes-no-dialog/yes-no-dialog.component';
import { PageHeaderModule } from '../../shared';
import { MatDialogModule } from '@angular/material/dialog';
import { DeleteDialogModule } from '../../shared/modules/dialog/delete-dialog/delete-dialog.module';
import { DeleteDialogComponent } from '../../shared/modules/dialog/delete-dialog/delete-dialog.component';
import {
    MatInputModule,
    MatPaginatorModule,
    MatProgressSpinnerModule,
    MatSortModule,
    MatTableModule,
    MatCheckboxModule,
    MatButtonModule,
    MatIconModule,
    MatSlideToggleModule,
    MatSelectModule
} from '@angular/material';
import { PlaceListComponent } from './place-list/place-list.component';
import { PlaceEditorComponent } from './place-editor/place-editor.component';
import { PlaceCreatorComponent } from './place-creator/place-creator.component';

@NgModule({
    imports: [
        CommonModule,
        PlaceManagerRoutingModule,
        CommonModule,
        PageHeaderModule,
        MatInputModule,
        MatTableModule,
        MatIconModule,
        MatPaginatorModule,
        MatSortModule,
        MatProgressSpinnerModule,
        MatCheckboxModule,
        MatButtonModule,
        MatDialogModule,
        MatSlideToggleModule,
        DeleteDialogModule,
        AlertDialogModule,
        YesNoDialogModule,
        FormsModule,
        ReactiveFormsModule,
        MatSelectModule,
        NgxMatSelectSearchModule
    ],
    entryComponents: [
        DeleteDialogComponent,
        AlertDialogComponent,
        YesNoDialogComponent
    ],
    declarations: [
        PlaceManagerComponent,
        PlaceListComponent,
        PlaceEditorComponent,
        PlaceCreatorComponent
    ]
})
export class PlaceManagerModule { }
