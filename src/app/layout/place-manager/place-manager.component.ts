import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../router.animations';

@Component({
  selector: 'app-place-manager',
  templateUrl: './place-manager.component.html',
  styleUrls: ['./place-manager.component.scss']
})
export class PlaceManagerComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
