import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  AfterViewInit,
  OnDestroy
} from '@angular/core';
import { routerTransition } from '../../../router.animations';
import { ActivatedRoute } from '@angular/router';
import { PlaceService } from '../../../shared/services/place/place.service';
import { toInteger } from '@ng-bootstrap/ng-bootstrap/util/util';
import { catchError } from 'rxjs/internal/operators/catchError';
import { finalize } from 'rxjs/internal/operators/finalize';
import { of, BehaviorSubject } from 'rxjs';
import { Place } from '../../../models/place.model';
import { MatDialog } from '@angular/material';
import { AlertDialogComponent } from '../../../shared/modules/dialog/alert-dialog/alert-dialog.component';
import { YesNoDialogComponent } from '../../../shared/modules/dialog/yes-no-dialog/yes-no-dialog.component';
import {
  FormGroup,
  Validators,
  FormBuilder
} from '@angular/forms';
import * as apiPath from '../../../shared/global/api-path';

@Component({
  selector: 'app-place-editor',
  templateUrl: './place-editor.component.html',
  styleUrls: ['./place-editor.component.scss'],
  animations: [routerTransition()]
})

export class PlaceEditorComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild('input') input: ElementRef;
  placeEditorForm: FormGroup;
  id: number;
  place: Place;
  floorPlanUrl: any;
  activeStatus: string;

  constructor(private placeService: PlaceService,
    private _matDialog: MatDialog,
    private route: ActivatedRoute,
    private fb: FormBuilder) {
    this.activeStatus = 'Inactive';
    this.createForm(null);
  }

  private placesSubject = new BehaviorSubject<Place>(null);
  private loadingSubject = new BehaviorSubject<boolean>(false);
  public loading$ = this.loadingSubject.asObservable();

  ngOnInit() {
    this.id = toInteger(this.route.snapshot.paramMap.get('id'));
    this.getPlaceById(this.id);

    this.placesSubject.subscribe((res) => {
        this.place = res;
        this.createForm(res);
    });
  }

  ngAfterViewInit(): void {
  }

  getPlaceById(id: number) {
    this.loadingSubject.next(true);
    this.placeService.getPlaceById(id).pipe(
      catchError(() => of([])),
      finalize(() => {
        this.loadingSubject.next(false);
      })
    ).subscribe((place) => {
      this.placesSubject.next(place as Place);
    });
  }

  onActiveChange(value) {
    if (value.checked === true) {
      this.activeStatus = 'Active';
    } else {
      this.activeStatus = 'Inactive';
    }
  }


  createForm(place: Place) {
    if (place !== null) {
      this.placeEditorForm = this.fb.group({
        'name': [place.name, Validators.required],
        'address': [place.address, Validators.required],
        'postcode': [place.postcode],
        'tel': [place.tel, Validators.required],
        'email': [place.email, [
          Validators.required,
          // tslint:disable-next-line:max-line-length quotemark
          Validators.pattern("[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?")]
        ],
        'website': [place.website],
        'latitude': [place.latitude],
        'longitude': [place.longitude],
        'floorplan': [place.floorplan],
        'isActive': (place.status === 'A' ? true : false)
      });

      if (place.floorplan !== undefined && place.floorplan !== null) {
        this.floorPlanUrl = apiPath.getFullPath(place.floorplan.toString());
      }

      if (place.status === 'A' ? true : false) {
        this.activeStatus = 'Active';
      } else {
        this.activeStatus = 'Inactive';
      }

    } else {
      this.placeEditorForm = this.fb.group({
        'name': [null, Validators.required],
        'address': [null, Validators.required],
        'postcode': [null],
        'tel': ['', Validators.required],
        'email': ['', [
          Validators.required,
          // tslint:disable-next-line:max-line-length quotemark
          Validators.pattern("[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?")]
        ],
        'website': [null],
        'latitude': [null],
        'longitude': [null],
        'floorplan': [null],
        'isActive': [false]
      });
      this.floorPlanUrl = null;
      this.activeStatus = 'Inactive';
    }
  }

  readUrl(event: any) {
    if (event.target.files && event.target.files[0]) {
      const reader = new FileReader();
      // tslint:disable-next-line:no-shadowed-variable
      reader.onload = (event: any) => {
        this.floorPlanUrl = event.target.result;
      };

      reader.readAsDataURL(event.target.files[0]);
      const file = event.target.files[0];
      this.placeEditorForm.get('floorplan').setValue(file);
    }
  }

  edit(value: any) {
    const placeEdit = value as Place;
    if (value.isActive) {
      placeEdit.status = 'A';
    } else {
      placeEdit.status = 'D';
    }

    placeEdit.id = this.place.id;
    this.place = placeEdit;

    if (this.place !== null) {
      const confirmDialog = this._matDialog.open(YesNoDialogComponent, {
        data: { head: 'Warning.', content: 'Do you want to edit ' + this.place.name + '.' }
      });

     confirmDialog.afterClosed().subscribe(result => {
       if (result === 'yes') {
          this.placeService.edit(this.place).pipe(
            catchError((res) => of([this._matDialog.open(AlertDialogComponent, {
              data: { head: 'Error.', content: 'Cannot Edit ' + this.place.name + ' .', description: res.error }
            })])),
            finalize(() => {
              this.loadingSubject.next(false);
            })
          ).subscribe((res) => {
            if ((res as Place).name !== undefined) {
                this._matDialog.open(AlertDialogComponent, {
                  data: { head: 'Complete.', content: 'Edit ' + (res as Place).name + ' Complete.' }
                });
              this.placesSubject.next((res as Place));
            }
          });
          this.loadingSubject.next(true);
        }
      });
    }
  }

  reset() {
    this.createForm(this.place);
  }

  ngOnDestroy(): void {
    this.placesSubject.unsubscribe();
    this.loadingSubject.unsubscribe();
  }
}
