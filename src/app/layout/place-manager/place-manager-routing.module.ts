import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PlaceManagerComponent } from './place-manager.component';
import { PlaceListComponent } from './place-list/place-list.component';
import { PlaceEditorComponent } from './place-editor/place-editor.component';
import { PlaceCreatorComponent } from './place-creator/place-creator.component';

const routes: Routes = [
  {
    path: '',
    component: PlaceManagerComponent,
    children: [
      { path: '', redirectTo: 'place-list' },
      { path: 'place-list', component: PlaceListComponent },
      { path: 'place-creator', component: PlaceCreatorComponent },
      { path: 'place-editor/:id', component: PlaceEditorComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PlaceManagerRoutingModule { }
