import { Component, OnInit, ViewChild, ElementRef, OnDestroy } from '@angular/core';
import { PlaceService } from '../../../shared/services/place/place.service';
import { MatDialog } from '@angular/material';
import { Place } from '../../../models/place.model';
import { BehaviorSubject, of } from 'rxjs';
import { YesNoDialogComponent } from '../../../shared/modules/dialog/yes-no-dialog/yes-no-dialog.component';
import { catchError, finalize } from 'rxjs/operators';
import { AlertDialogComponent } from '../../../shared/modules/dialog/alert-dialog/alert-dialog.component';
import { FormGroup, Validators, FormBuilder} from '@angular/forms';
import * as apiPath from '../../../shared/global/api-path';

@Component({
  selector: 'app-place-creator',
  templateUrl: './place-creator.component.html',
  styleUrls: ['./place-creator.component.scss']
})
export class PlaceCreatorComponent implements OnInit, OnDestroy {

  @ViewChild('input') input: ElementRef;
  placeCreatorForm: FormGroup;
  id: number;
  place: Place;
  floorPlanUrl: any;
  activeStatus: string;

  private placesSubject = new BehaviorSubject<Place>(null);
  private loadingSubject = new BehaviorSubject<boolean>(false);
  public loading$ = this.loadingSubject.asObservable();
  constructor(private placeService: PlaceService, private _matDialog: MatDialog,  private fb: FormBuilder) {
    this.activeStatus = 'Inactive';
    this.createForm(null);
  }


  ngOnInit() {
    this.placesSubject.subscribe((res) => {
      this.place = (res);
      this.createForm(res);
    });
  }

  onActiveChange(value) {
    if (value.checked === true) {
      this.activeStatus = 'Active';
    } else {
      this.activeStatus = 'Inactive';
    }
  }

  createForm(place: Place) {
    if (place !== null) {
      this.placeCreatorForm = this.fb.group({
        'name': [place.name, Validators.required],
        'address': [place.address, Validators.required],
        'postcode': [place.postcode],
        'tel': [place.tel, Validators.required],
        'email': [place.email, [
          Validators.required,
          // tslint:disable-next-line:max-line-length quotemark
          Validators.pattern("[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?")]
        ],
        'website': [place.website],
        'latitude': [place.latitude],
        'longitude': [place.longitude],
        'floorplan': [place.floorplan],
        'isActive': (place.status === 'A' ? true : false)
      });
      if (this.place.floorplan !== null && this.place.floorplan !== undefined) {
        this.floorPlanUrl = apiPath.getFullPath(this.place.floorplan.toString());
      }

      if (place.status === 'A' ? true : false) {
        this.activeStatus = 'Active';
      } else {
        this.activeStatus = 'Inactive';
      }

    } else {
      this.placeCreatorForm = this.fb.group({
        'name': [null, Validators.required],
        'address': [null, Validators.required],
        'postcode': [null],
        'tel': ['', Validators.required],
        'email': ['', [
          Validators.required,
          // tslint:disable-next-line:max-line-length quotemark
          Validators.pattern("[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?")]
        ],
        'website': [null],
        'latitude': [null],
        'longitude': [null],
        'floorplan': [null],
        'isActive': [false],
      });
      this.floorPlanUrl = null;
      this.activeStatus = 'Inactive';
    }
  }

  reset() {
    this.createForm(null);
  }

  readUrl(event: any) {
    if (event.target.files && event.target.files[0]) {
      const reader = new FileReader();
      // tslint:disable-next-line:no-shadowed-variable
      reader.onload = (event: any) => {
        this.floorPlanUrl = event.target.result;
      };

      reader.readAsDataURL(event.target.files[0]);
        const file = event.target.files[0];
        this.placeCreatorForm.get('floorplan').setValue(file);
    }
  }

  onCreateClick(value: any) {
    const placeAdd = value as Place;
    if (value.isActive) {
      placeAdd.status = 'A';
    } else {
      placeAdd.status = 'D';
    }

    this.place = placeAdd;
    if (this.place != null) {
      this._matDialog.open(YesNoDialogComponent, {
        data: { head: 'Warning.', content: 'Do you want to create ' + this.place.name + '?' }
      }).afterClosed().subscribe(result => {
        if (result === 'yes') {
          this.loadingSubject.next(true);
          this.placeService.create(this.place).pipe(
            catchError((res) => of([
              this._matDialog.open(AlertDialogComponent, {
                data: { head: 'Error.', content: 'Cannot Create ' + this.place.name + ' .', description: res.error }
              })
            ])),
            finalize(() => {
              this.loadingSubject.next(false);
            })
          ).subscribe((res) => {
              if ((res as Place).name !== undefined) {
              this._matDialog.open(AlertDialogComponent, {
                data: { head: 'Complete.', content: 'Create ' + (res as Place).name + ' Complete.' }
              });
            }
            this.placesSubject.next((res as Place));
          });
        }
      });
    }
  }

  ngOnDestroy(): void {
    this.loadingSubject.complete();
    this.loadingSubject.unsubscribe();
    this.placesSubject.complete();
    this.placesSubject.unsubscribe();
  }
}
