export interface Thumb {
    id: number;
    group: string;
    refId: number;
    path: string;
    url: string;
}
