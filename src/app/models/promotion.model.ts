import { Thumb } from './thumb.model';

export interface Promotion {
    id: number;
    eventId: number;
    boothId: number;
    name: string;
    description: string;
    status: string;
    thumb: Thumb[];
}

export interface PromotionWithPagination {
    current_page: number;
    data: Promotion[];
    from: number;
    last_page: number;
    next_page_url: number;
    path: string;
    per_page: string;
    prev_page_url: string;
    to: number;
    total: number;
}
