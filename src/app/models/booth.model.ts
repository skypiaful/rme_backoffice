import { Promotion } from './promotion.model';

export interface Booth {
    id: number;
    eventId: number;
    name: string;
    no: string;
    top: number;
    left: number;
    status: string;
    created_at: Date;
    updated_at: Date;
    promotions: Promotion[];
}

export interface BoothWithPagination {
    current_page: number;
    data: Booth[];
    from: number;
    last_page: number;
    next_page_url: number;
    path: string;
    per_page: string;
    prev_page_url: string;
    to: number;
    total: number;
}
