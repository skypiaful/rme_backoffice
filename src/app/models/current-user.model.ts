export class CurrentUser {
    id: number;
    roleId: number;
    username: string;
    email: string;
    firstName: string;
    lastName: string;
    facebookId: string;
    googleId: string;
    phoneNo: string;
    lastLogin: string;
    created_at: string;
    updated_at: string;
    token: string;
}
