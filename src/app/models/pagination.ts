export interface Pagination<T> {
    current_page: number;
    data: T[];
    from: number;
    last_page: number;
    next_page_url: number;
    path: string;
    per_page: string;
    prev_page_url: string;
    to: number;
    total: number;
}
