import { Gallery } from './gallery.model';
import { Booth } from './booth.model';

export interface Event {
        id: number;
        placeId: number;
        name: string;
        description: string;
        icon: string;
        publishDate: Date;
        expireDate: Date;
        status: string;
        created_at: Date;
        updated_at: Date;
        url: string;
        galleries: Gallery[];
        booths: Booth[];
}

export interface EventWithPagination {
    current_page: number;
    data: Event[];
    from: number;
    last_page: number;
    next_page_url: number;
    path: string;
    per_page: string;
    prev_page_url: string;
    to: number;
    total: number;
}
