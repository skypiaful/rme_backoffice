export interface Gallery {
    id: number;
    group: string;
    refId: number;
    path: number;
}
