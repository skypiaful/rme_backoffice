export interface Place {
    id: number;
    name: string;
    address: string;
    postcode: string;
    tel: string;
    email: string;
    website: string;
    latitude: number;
    longitude: number;
    floorplan: File;
    icon: string;
    status: string;
    created_at: Date;
    updated_at: Date;
}

export interface PlaceWithPagination {
    current_page: number;
    data: Place[];
    from: number;
    last_page: number;
    next_page_url: number;
    path: string;
    per_page: string;
    prev_page_url: string;
    to: number;
    total: number;
}
