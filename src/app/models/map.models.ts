export interface Map {
    id: number;
    group: string;
    refId: number;
    sub: string;
    path: string;
    name: string;
    tmp_name: string;
    extension: string;
    error: string;
    size: number;
    created_at: Date;
    updated_at: Date;
    url: string;
}
