export interface User {
    id: number;
    username: string;
    password: string;
    firstName: string;
    lastName: string;
    phoneNo: string;
    email: string;
    avatar: string;
}

