import { Component, OnInit, Inject, ElementRef } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef, MatDialogConfig, MatDialog } from '@angular/material';
import { Place } from '../../../../models/place.model';
@Component({
  selector: 'app-delete-dialog',
  templateUrl: './delete-dialog.component.html',
  styleUrls: ['./delete-dialog.component.scss']
})
export class DeleteDialogComponent implements OnInit {
  constructor(
    public dialogRef: MatDialogRef<DeleteDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public place: Place) {
  }

  ngOnInit() {
  }
  onCloseConfirm() {
    this.dialogRef.close(this.place);
  }
  onCloseCancel() {
    this.dialogRef.close('Cancel');
  }
}
