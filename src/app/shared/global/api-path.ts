'use strict';
import { environment } from '../../../environments/environment';
export const PLACE = `${environment.baseUrl}/api/place`;
export const LOGIN = `${environment.baseUrl}/api/auth/login`;
export const EVENT = `${environment.baseUrl}/api/event`;
export const PLACEUPLOAD = `${environment.baseUrl}/api/place/upload`;
export const BOOTH = `${environment.baseUrl}/api/booth`;
export const USER = `${environment.baseUrl}/api/user`;
export const PROMOTION = `${environment.baseUrl}/api/promotion`;

export function getFullPath(childPath: string): string {
    return `${environment.baseUrl}${childPath}`;
}
