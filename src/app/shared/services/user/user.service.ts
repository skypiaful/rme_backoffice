import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { map, timeout, finalize } from 'rxjs/operators';
import { User } from '../../../models/user.model';
import { Pagination } from '../../../models/pagination';
import { HttpParams, HttpClient } from '@angular/common/http';
import { environment } from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})

export class UserService {

  public onUsersChanged: BehaviorSubject<Pagination<User>> = new BehaviorSubject(null);
  public onUserChanged: BehaviorSubject<User> = new BehaviorSubject(null);
  public onLoading: BehaviorSubject<boolean> = new BehaviorSubject(false);

  private loadingSubject = new BehaviorSubject<boolean>(false);

  public criteria: any;

  public loading$ = this.loadingSubject.asObservable();

  constructor(private http: HttpClient) {
    this.criteria = { 'page': 1, 'per_page': 20 };
  }

  public getUsers(): Observable<Pagination<User>> {
    this.loadingSubject.next(true);

    return this.http.get(environment.apiUrl + 'user', { params: this.criteria }).pipe(
      finalize(() => {
        this.loadingSubject.next(false);
      }),
      map((res) => {
        const page = res as Pagination<User>;
        this.onUsersChanged.next(page);
        return page;
      })
    );
  }

  create(user: User) {
    this.loadingSubject.next(true);

    const url = environment.apiUrl + 'user/' + user.id;
    return this.http.post(url, user).pipe(
      finalize(() => {
        this.loadingSubject.next(false);
      }),
      map((res) => {
        return res;
      }),
      timeout(environment.timeout)
    );
  }

  update(user: User) {
    this.loadingSubject.next(true);

    const url = environment.apiUrl + 'user/' + user.id;
    return this.http.put(url, user).pipe(
      finalize(() => {
        this.loadingSubject.next(false);
      }),
      map((res) => {
        return res;
      }),
      timeout(environment.timeout)
    );
  }

  public delete(id: number): Observable<any> {
    const url = environment.apiUrl + 'user';
    return this.http.delete(url).pipe(
      map((res) => {
        return res;
      }),
      timeout(environment.timeout)
    );
  }

  private buildParams(criteria: UserCriteria) {
    const params = new HttpParams();

    params.append('f', 'p');
    params.append('term', criteria.term);
    params.append('sort', criteria.sort);
    params.append('direction', criteria.direction);
    params.append('page', criteria.page + '');
    params.append('per_page', criteria.per_page + '');

    return params;
  }

  postFile(userId, fileToUpload: File): Observable<any> {
    const url = environment.apiUrl + 'user/upload/' + userId;
    const formData: FormData = new FormData();
    formData.append('avatar[]', fileToUpload, fileToUpload.name);
    return this.http.post(url, formData).pipe(
      map((res) => {
        return res;
      }),
      timeout(environment.timeout)
    );
  }

}

export class UserCriteria {
  term: string;
  sort: string;
  direction: string;
  page: number;
  per_page: number;

  UserCriteria(data) {
    this.term = '';
    this.sort = '';
    this.direction = '';
    this.page = 1;
    this.per_page = 20;
  }
}
