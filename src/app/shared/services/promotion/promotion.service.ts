import { Injectable } from '@angular/core';
import { Promotion, PromotionWithPagination } from '../../../models/promotion.model';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, timeout } from 'rxjs/operators';
import * as apiPath from '../../global/api-path';

@Injectable()
export class PromotionService {
  constructor(private http: HttpClient) { }
  url = apiPath.PROMOTION;
  interval_timeout = 20000;

  public getPromotions(
    filter: string = '',
    sortCollumn: string,
    direction: string = 'asc',
    page_index: number,
    page_size: number): Observable<PromotionWithPagination> {
    let Params = new HttpParams();
    Params = Params.append('f', 'p');
    Params = Params.append('term', filter);
    Params = Params.append('sort', sortCollumn);
    Params = Params.append('direction', direction);
    Params = Params.append('page', page_index.toString());
    Params = Params.append('per_page', page_size.toString());
    page_index++;
    return this.http.get(this.url, { params: Params }).pipe(
      map((res) => {
        const promotions = res as PromotionWithPagination;
        return promotions;
      }),
      timeout(this.interval_timeout)
    );
  }

  public getPromotionById(promotionId: number): Observable<Promotion> {
    const url = `${this.url}/${promotionId}`;
    return this.http.get(url).pipe(
      map((res) => {
        return (res as Promotion);
      }),
      timeout(this.interval_timeout)
    );
  }

  public create(promotion: Promotion): Observable<Promotion> {
    const url = this.url;
    const data = JSON.stringify(promotion);
    return this.http.post(url, promotion).pipe(
      map((res) => {
        return (res as Promotion);
      }),
      timeout(this.interval_timeout)
    );
  }

  public edit(promotion: Promotion): Observable<Promotion> {
    const url = `${this.url}/${promotion.id}`;
    const data = JSON.stringify(promotion);
    return this.http.put(url, promotion).pipe(
      map((res) => {
        return (res as Promotion);
      }),
      timeout(this.interval_timeout)
    );
  }

  public delete(id: number): Observable<any> {
    const url = `${this.url}/${id}`;
    return this.http.delete(url).pipe(
      map((res) => {
        return res;
      }),
      timeout(this.interval_timeout)
    );
  }
}
