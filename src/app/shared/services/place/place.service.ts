import { Injectable } from '@angular/core';
import { Place, PlaceWithPagination } from '../../../models/place.model';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Observable, merge, of } from 'rxjs';
import { map, timeout, flatMap, mergeMap, mergeAll } from 'rxjs/operators';
import * as apiPath from '../../global/api-path';

@Injectable()
export class PlaceService {
  progress$: any;
  progressObserver: any;
  progress: number;
  constructor(private http: HttpClient) {
  }

  placeApi = apiPath.PLACE;
  interval_timeout = 20000;

  public getPlaces(
    filter: string = '',
    sortCollumn: string,
    direction: string = 'asc',
    page_index: number,
    page_size: number): Observable<PlaceWithPagination> {

    let Params = new HttpParams();
    Params = Params.append('f', 'p');
    Params = Params.append('term', filter);
    Params = Params.append('sort', sortCollumn);
    Params = Params.append('direction', direction);
    page_index++;
    Params = Params.append('page', page_index.toString());
    Params = Params.append('per_page', page_size.toString());

    return this.http.get(this.placeApi, { params: Params }).pipe(
      map((res) => {
        const places = res as PlaceWithPagination;
        return places;
      }),
      timeout(this.interval_timeout)
    );
  }

  public getPlaceById(placeId: number): Observable<Place> {
    const url = `${this.placeApi}/${placeId}`;
    return this.http.get(url).pipe(
      map((res) => {
        return (res as Place);
      }),
      timeout(this.interval_timeout)
    );
  }

  public create(place: Place): Observable<Place> {
    const floorPlan: File = place.floorplan;
    // formData.append('floorplan[]', (res as Place).floorplan, `test.png`);
    //     return this.http.post(`${apiPath.PLACEUPLOAD}/${(res as Place).id}`, formData).pipe(
    //       map(() => {
    //         return (res as Place);
    //       });
    //     );
    return this.http.post(this.placeApi, place).pipe(
      map((res) => {
        return res as Place;
      }));
  }

  public edit(place: Place): Observable<Place> {
    const url = `${this.placeApi}/${place.id}`;
    const formData: FormData = new FormData();
    formData.append('floorplan[]', place.floorplan, `${place.id}.png`);
    if (place.floorplan !== undefined && place.floorplan !== null) {
      return this.http.post(`${apiPath.PLACEUPLOAD}/${place.id}`, formData).pipe(
        map((res) => {
          return null;
        })
        // flatMap(() => {
        //   return this.http.put(url, place).pipe(
        //     map((res) => {
        //       return (res as Place);
        //     }));
        // })
      );
    } else {
      return this.http.put(url, place).pipe(
        map((res) => {
          return (res as Place);
        }));
    }
  }

  public delete(id: number): Observable<any> {
    const url = `${this.placeApi}/${id}`;
    return this.http.delete(url).pipe(
      map((res) => {
        return res;
      }),
      timeout(this.interval_timeout)
    );
  }
}
