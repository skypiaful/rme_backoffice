import { CollectionViewer, DataSource } from '@angular/cdk/collections';
import { Place, PlaceWithPagination } from '../../../models/place.model';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { PlaceService } from './place.service';
import { catchError, finalize } from 'rxjs/operators';
import { MatPaginator } from '@angular/material';

export class PlaceDataSource implements DataSource<any> {

    public placesSubject = new BehaviorSubject<any>([]);
    public loadingSubject = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingSubject.asObservable();

    private active = 'id';

    constructor(private placeService: PlaceService) { }

    connect(collectionViewer: CollectionViewer): Observable<Place[]> {
        return this.placesSubject.asObservable();
    }

    disconnect(collectionViewer: CollectionViewer): void {
        this.placesSubject.complete();
        this.loadingSubject.complete();
    }

    loadPlaces(filter: string, active: string, sortDirection: string, pageIndex: number, pageSize: number, paginator: MatPaginator = null) {
        this.loadingSubject.next(true);

        this.placeService.getPlaces(filter, active, sortDirection, pageIndex, pageSize).pipe(
            catchError((e: any) => Observable.throw(this.errorHandler(e))),
            finalize(() => {
                this.loadingSubject.next(false);
            })
        ).subscribe((places) => {
            const place = (places as PlaceWithPagination);
            if (paginator != null && place != null) {
                paginator.length = place.total;
                this.placesSubject.next(place.data);
            } else {
                this.placesSubject.next(null);
            }
        });
    }

    delete(id: number): Observable<any> {
        this.loadingSubject.next(true);

        return this.placeService.delete(id).pipe(
            finalize(() => {
                this.loadingSubject.next(false);
            })
        );
    }

    loadPlacesWithPaginator(filter: string, active: string, sortDirection: string, paginator: MatPaginator) {
        this.loadPlaces(
            filter,
            active,
            sortDirection,
            paginator.pageIndex,
            paginator.pageSize,
            paginator
        );
    }

    errorHandler(error: any): void {
        console.log(error);
      }
}
