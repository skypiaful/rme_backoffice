import { CollectionViewer, DataSource } from '@angular/cdk/collections';
import { Event, EventWithPagination } from '../../../models/event.model';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { EventService } from './event.service';
import { catchError, finalize } from 'rxjs/operators';
import { MatPaginator } from '@angular/material';

export class EventDataSource implements DataSource<any> {

    public eventsSubject = new BehaviorSubject<any>([]);
    public loadingSubject = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingSubject.asObservable();

    private active = 'id';

    constructor(private eventService: EventService) { }

    connect(collectionViewer: CollectionViewer): Observable<Event[]> {
        return this.eventsSubject.asObservable();
    }

    disconnect(collectionViewer: CollectionViewer): void {
        this.eventsSubject.complete();
        this.loadingSubject.complete();
    }

    loadEvents(filter: string, active: string, sortDirection: string, pageIndex: number, pageSize: number, paginator: MatPaginator = null) {
        this.loadingSubject.next(true);

        this.eventService.getEvents(filter, active, sortDirection, pageIndex, pageSize).pipe(
            catchError((e: any) => Observable.throw(e)),
            finalize(() => {
                this.loadingSubject.next(false);
            })
        ).subscribe((events) => {
            const event = (events as EventWithPagination);
            if (paginator != null && event != null) {
                paginator.length = event.total;
                this.eventsSubject.next(event.data);
            } else {
                this.eventsSubject.next(null);
            }
        });
    }

    delete(id: number): Observable<any> {
        this.loadingSubject.next(true);

        return this.eventService.delete(id).pipe(
            finalize(() => {
                this.loadingSubject.next(false);
            })
        );
    }

    loadEventsWithPaginator(filter: string, active: string, sortDirection: string, paginator: MatPaginator) {
        this.loadEvents(
            filter,
            active,
            sortDirection,
            paginator.pageIndex,
            paginator.pageSize,
            paginator
        );
    }

    errorHandler(error: any): void {
        console.log(error);
      }
}
