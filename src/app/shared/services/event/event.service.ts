import { Injectable } from '@angular/core';
import { Event, EventWithPagination } from '../../../models/event.model';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import { map, timeout } from 'rxjs/operators';
import * as apiPath from '../../global/api-path';

@Injectable()
export class EventService {
  constructor(private http: HttpClient) { }
  url = apiPath.EVENT;
  interval_timeout = 20000;

  public getEvents(
    filter: string = '',
    sortCollumn: string,
    direction: string = 'asc',
    page_index: number,
    page_size: number): Observable<EventWithPagination> {
    let Params = new HttpParams();
    Params = Params.append('f', 'p');
    Params = Params.append('term', filter);
    Params = Params.append('sort', sortCollumn);
    Params = Params.append('direction', direction);
    page_index++;
    Params = Params.append('page', page_index.toString());
    Params = Params.append('per_page', page_size.toString());

    return this.http.get(this.url, { params: Params }).pipe(
      map((res) => {
        const events = res as EventWithPagination;
        return events;
      }),
      timeout(this.interval_timeout)
    );
  }

  public getEventById(eventId: number): Observable<Event> {
    const url = `${this.url}/${eventId}`;
    return this.http.get(url).pipe(
      map((res) => {
        return (res as Event);
      }),
      timeout(this.interval_timeout)
    );
  }

  public create(event: Event): Observable<Event> {
    const url = this.url;
    const data = JSON.stringify(event);
    return this.http.post(url, event).pipe(
      map((res) => {
        return (res as Event);
      }),
      timeout(this.interval_timeout)
    );
  }

  public edit(event: Event): Observable<Event> {
    const url = `${this.url}/${event.id}`;
    const data = JSON.stringify(event);
    return this.http.put(url, event).pipe(
      map((res) => {
        return (res as Event);
      }),
      timeout(this.interval_timeout)
    );
  }

  public delete(id: number): Observable<any> {
    const url = `${this.url}/${id}`;
    return this.http.delete(url).pipe(
      map((res) => {
        return res;
      }),
      timeout(this.interval_timeout)
    );
  }
}
