import { Injectable } from '@angular/core';
import { Booth, BoothWithPagination } from '../../../models/booth.model';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, timeout } from 'rxjs/operators';
import * as apiPath from '../../global/api-path';

@Injectable()
export class BoothService {
  constructor(private http: HttpClient) { }
  url = apiPath.BOOTH;
  interval_timeout = 20000;

  public getBooths(
    filter: string = '',
    sortCollumn: string,
    direction: string = 'asc',
    page_index: number,
    page_size: number): Observable<BoothWithPagination> {
    let Params = new HttpParams();
    Params = Params.append('f', 'p');
    Params = Params.append('term', filter);
    Params = Params.append('sort', sortCollumn);
    Params = Params.append('direction', direction);
    Params = Params.append('page', page_index.toString());
    Params = Params.append('per_page', page_size.toString());
    page_index++;
    return this.http.get(this.url, { params: Params }).pipe(
      map((res) => {
        const booths = res as BoothWithPagination;
        return booths;
      }),
      timeout(this.interval_timeout)
    );
  }

  public getBoothById(boothId: number): Observable<Booth> {
    const url = `${this.url}/${boothId}`;
    return this.http.get(url).pipe(
      map((res) => {
        return (res as Booth);
      }),
      timeout(this.interval_timeout)
    );
  }

  public create(booth: Booth): Observable<Booth> {
    const url = this.url;
    const data = JSON.stringify(booth);
    return this.http.post(url, booth).pipe(
      map((res) => {
        return (res as Booth);
      }),
      timeout(this.interval_timeout)
    );
  }

  public edit(booth: Booth): Observable<Booth> {
    const url = `${this.url}/${booth.id}`;
    const data = JSON.stringify(booth);
    return this.http.put(url, booth).pipe(
      map((res) => {
        return (res as Booth);
      }),
      timeout(this.interval_timeout)
    );
  }

  public delete(id: number): Observable<any> {
    const url = `${this.url}/${id}`;
    return this.http.delete(url).pipe(
      map((res) => {
        return res;
      }),
      timeout(this.interval_timeout)
    );
  }
}
