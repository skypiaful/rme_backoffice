import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { User } from '../../models/user.model';
import { Observable, of } from 'rxjs';
import { map, catchError, finalize, timeout } from 'rxjs/operators';
import { CurrentUser } from '../../models/current-user.model';
import * as apiPath from '../global/api-path';

@Injectable()
export class AuthenticationService {
    constructor(private http: HttpClient) { }
    login(username: string, password: string): Observable<CurrentUser> {
        const apiUrl = apiPath.LOGIN;
        return this.http.post<any>(apiUrl,
            {
                email: username,
                password: password
            }).pipe(
                timeout(5000),
                map(user => {
                    if (user && user.token) {
                        // store user details and jwt token in local storage to keep user logged in between page refreshes
                        localStorage.setItem('currentUser', JSON.stringify(user));
                        return user;
                    }

                }));
    }

    logout() {
        // remove user from local storage to log user out
        localStorage.removeItem('currentUser');
    }
}
